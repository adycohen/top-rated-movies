import { Component, OnInit, Inject } from '@angular/core';
import { MoviesDataService } from '../movies-data.service';
import { MatDialog, MatDialogConfig } from "@angular/material";

import { MovieDetailsComponent } from '../movie-details/movie-details.component';


@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {
  moviesList = [];
  movie;
  pageNumber:number;
  enableNextButton:boolean;
  enablePrevButton:boolean;


  constructor(@Inject(MoviesDataService) private moviesService:MoviesDataService,
  private dialog: MatDialog) {

    this.enableNextButton = true;
    this.enablePrevButton = false;
    this.pageNumber=1;
   }

  ngOnInit() {
    this.moviesService.getTopRatedMovies(this.pageNumber)
    .subscribe(data => {
      this.moviesList = data['results'];
    });
  }

  onNextPageClick() {
    window.scroll(0,0);
    this.pageNumber++;
    this.enablePrevButton = true;

    if (this.pageNumber <= 5) {
      this.moviesService.getTopRatedMovies(this.pageNumber)
      .subscribe(data => {
        this.moviesList = data['results'];
      });  
    }

    if (this.pageNumber == 5) {
      this.enableNextButton = false;
    }
  }

  onPrevPageClick() {
    window.scroll(0,0);
    this.pageNumber--;
    this.enableNextButton = true;

    if (this.pageNumber >= 1) {
      this.moviesService.getTopRatedMovies(this.pageNumber)
      .subscribe(data => {
        this.moviesList = data['results'];
      });
    }

    if (this.pageNumber == 1) {
      this.enablePrevButton = false;
    }
  }

  openMovieDetails(index:number) {
    this.movie = this.moviesList[index];
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = { 
      id: this.movie.id,
      title: this.movie.title,
      originalTitle: this.movie.original_title,
      backdropPath: this.movie.backdrop_path,
      posterPath: this.movie.poster_path,
      popularity: this.movie.popularity,
      overview: this.movie.overview
    };

    this.dialog.open(MovieDetailsComponent, dialogConfig); 
  }
}
