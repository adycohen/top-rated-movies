import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MoviesDataService {

  constructor(@Inject(HttpClient) private http:HttpClient) { }

  getTopRatedMovies(pageNumber:number) {
    return this.http.get(
      "https://api.themoviedb.org/3/movie/top_rated?api_key=a9725e2518ce7ad35ac09689927a5b1a&page=" +
       pageNumber);
  }

  getRecommendedMovies(movieId:number) {
    return this.http.get(
      "https://api.themoviedb.org/3/movie/" + movieId + 
      "/recommendations?api_key=a9725e2518ce7ad35ac09689927a5b1a&page=1");
  }
}

