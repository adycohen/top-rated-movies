import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material";

import { MoviesDataService } from '../movies-data.service';


@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  id:number;
  title:string;
  originalTitle:string;
  popularity:number;
  overview:string;
  posterPath:string;
  backdropPath:string;
  recommendedMovies = [];

  showOriginalTitle:boolean;

  constructor(private moviesService:MoviesDataService,
    @Inject(MAT_DIALOG_DATA) data) {
      this.id = data.id;
      this.title = data.title;
      this.backdropPath = data.backdropPath;
      this.originalTitle = data.originalTitle;
      this.posterPath = data.posterPath;
      this.popularity = data.popularity;
      this.overview = data.overview;

      if(this.title != this.originalTitle){
        this.showOriginalTitle = true;
      } 
     }

  ngOnInit() {
    this.moviesService.getRecommendedMovies(this.id)
    .subscribe(data => {
      this.recommendedMovies = data['results'];
    });
  }
}
